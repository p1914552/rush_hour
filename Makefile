CPP_DIR = ./src/
CPP_SRC = $(CPP_DIR)situation.cpp $(CPP_DIR)main.cpp $(CPP_DIR)voiture.cpp $(CPP_DIR)noeud.cpp $(CPP_DIR)arbre.cpp

EXE_DIR = ./bin/
LDPATH=-L /usr/X11R6/lib/

CXX=g++
LDFLAGS=$(LDPATH) -std=c++14 -lX11
CFLAGS=-g -O0 -Wall -Wextra

$(EXE_DIR)rush_hour: $(CPP_SRC) $(wildcard *.h) $(wildcard *.tpp)
	$(CXX) $(CPP_SRC) $(CFLAGS) $(LDFLAGS) -o $@

clean:
	-$(RM) *.o $(EXE_DIR)rush_hour

doxy:
	doxygen doc/config_doxy

clean_doc:
	rm -r doc/html
