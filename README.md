# Rush_Hour


## Description
Ce projet est un créateur et solveur du jeu Rush Hour. Plusieurs mode sont disponibles et seront détaillés plus tard dans le readme



## Installation
Tout a été réalisé sous les machines Fédora de la fac.

Pour installer ce projet veuillez suivre les étapes suivantes :

> Note: Ce projet requiert C++14 pour compiler. Me contacter si besoins d'une version sous C++11.

Clonez ce repository dans votre machine puis dans l'invité de commande à la racine du dossier: 

```
make
```

## Documentation

Pour génerer la documentation il suffit de taper cette ligne de commande à la racine
```
make doxy
```

La doc sera generée en format HTML a cet endroit `./doc/html`

Pour lancer la page de la documentation il vous suffira de faire 
```
firefox ./doc/html/index.html
```

## Utilisation
Une fois avoir compilé le programme. Plusieurs modes sont disponibles. Voici quelques exemples :

> Note: Toutes les lignes de commandes sont réalisés depuis la racine du dossier

Pour lancer le jeu en mode Parcours de la situation déjà modélisé en .txt il suffit de faire
```
./bin/Rush_Hour
```

On peut ensuite rajouter 2 arguments à cet executable.

### Premier Argument
Cet argument doit être un chiffre. Si le chiffre est 1 ça va lancer le mode Résolution de la situation dans le .txt
```
./bin/rush_hour 1
```

Enfin, un argument strictement superieur à 1 (2 et plus) vont créer une situation qui a comme résolution minimale au minimum le nombre de coup passé en argument 
Exemple : 
```
./bin/rush_hour 5
```
Creera une situation aléatoire qui a comme résolution minimale 5 coups (ça peut être 5 comme 25)
> Note : Attention, les situations étant crées aléatoiremement, demander une situation qui a comme résolution minimale de + de 10 coups peut entrainer une attente longue.

### Deuxième Argument
Le Deuxième argument ne reconnait que la chaine de caractère SVG.
Lorsqu'on lance l'éxecutable sans l'argument SVG, la résolution sera affichée dans le terminal. Avec l'argument SVG, un .svg sera créer dans le dossier `./ressource/image`

Exemple : 
```
./bin/rush_hour 6 SVG
```
Créera une situation qui se résoud en 6 coup minimum. Et créera un .svg qui montrera la résolution pas à pas

```
./bin/rush_hour 6 svg
```
Créera une situation qui se résoud en 6 coup minimum. Mais utilisera le terminal pour pour l'affichage.

## Contact
Si questions veuillez me contactez sur l'adresse mail suivante : darius.hakim@etu.univ-lyon1.fr


## Auteurs
Utilisation d'une library header only pour generer le SVG : https://vincentlaucsb.github.io/svg/ (`./src/svg.hpp`).J'ai modifié la fonction frame_animate() de cette librairie pour pouvoir afficher à l'endroit mes situations

Je suis l'auteur du reste du code

## Problèmes connus
Utilisation trop importante de la mémoire. La cause de cette utilisation est que pour créer les situations qui découlent d'une situation, je fais un new. Sauf que beaucoup de situations sont créer puis sont reconnues comme déjà parcourue donc sont jetés.
> Note : Il n'y a AUCUNE fuite de mémoire. Juste une utilisation trop conséquente de cette dernière.

Pour résoudre ce problème il faudrait que je ne fasse de new sur les situations que si elles n'ont pas déjà été explorée. J'ai essayé de faire celà mais il faudrait revoir toute l'architecture de mon programme. Après 4h d'essais j'ai eu plus de problèmes qu'autre chose j'ai donc decidé de garder la version fonctionnelle.


