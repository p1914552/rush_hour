#include "situation.h"
#include "noeud.h"
#include "arbre.h"
#include <iostream>
#include <fstream>
#include <string>
#include <cassert>
#include <stdlib.h>
#include <time.h>       

Situation::Situation(std::string path){
    for(int i = 0; i<6; i++){
        for(int j = 0; j<6; j++){
            tabJeu[i][j] = ' ';
            nbVoiture = 0;
        }
    }
    RemplirTabVoitureDepuisFichier(path);
    RemplirTabJeuDepuisTabVoiture();
}

Situation::Situation(){
    for(int i = 0; i<6; i++){
        for(int j = 0; j<6; j++){
            tabJeu[i][j] = ' ';
        }
    }
    nbVoiture = 0;
    CreationSortieEtVoitureASortir();
    CreationVoitures();
    //RemplirTabJeuDepuisTabVoiture();
}

Situation::Situation(const Situation &s){
    for(int i = 0; i<6; i++){
        for(int j = 0; j<6; j++){
            tabJeu[i][j] = s.tabJeu[i][j];
        }
    }
    nbVoiture = s.nbVoiture;
    for(int i = 0; i<nbVoiture; i++){
        tabVoiture[i] = new Voiture(s.tabVoiture[i]->PositionVoiture(), s.tabVoiture[i]->Longueur(), s.tabVoiture[i]->EstHorizontale());
    }
    voitureASortir = tabVoiture[0];
    sortie = s.sortie;
}
Situation::~Situation(){
    for(int i = 0; i<nbVoiture;i++){
        if(tabVoiture[i]!= nullptr){
            delete tabVoiture[i];
        }
    }
    voitureASortir = nullptr;
}

void Situation::Afficher(){
    for(int i = 0; i<6; i++){
        for(int j = 0; j<6; j++){
            if((i==sortie.ligne)&&(sortie.colonne==j)){
                std::cout<<" "<<tabJeu[i][j]<<"="<<" ";
            }else{
                std::cout<<" "<<tabJeu[i][j]<<" ";
            }
        }
        std::cout<<std::endl;
    }
    std::cout<<std::endl;
}
void Situation::AfficherSVG(){
    SVG::SVG root = getSVG();

    root.autoscale();
    std::ofstream outfile("./ressource/image/SituationBase.svg");
    std::cout<<"votre svg a été crée dans ./ressource/image/SituationBase.svg"<<std::endl;
    outfile << std::string(root);
}
void Situation::MettreSortieSVG(SVG::SVG &root){
    auto shapes= root.add_child<SVG::Group>();
    if(voitureASortir->EstHorizontale()){
        shapes->add_child<SVG::Rect>("sortie");
        root.get_element_by_id("sortie")
        ->set_attr("x", (sortie.colonne+1)*100 + 5*sortie.colonne)
        .set_attr("y", sortie.ligne*100 + 5*sortie.ligne)
        .set_attr("width", 20).set_attr("height", 100);
    }else{
        shapes->add_child<SVG::Rect>("sortie");
        root.get_element_by_id("sortie")
        ->set_attr("x", sortie.colonne*100 + 5*sortie.colonne)
        .set_attr("y", sortie.ligne*100 + 5*sortie.ligne)
        .set_attr("width", 100).set_attr("height", -20);
    }
}

void Situation::MettreVoitureSVG(SVG::SVG &root, double x, double y, double width, double height, int numeroV){
        auto shapes= root.add_child<SVG::Group>();
        if(numeroV == 0){
            //voiture à sortir
            shapes->add_child<SVG::Rect>("voitureASortir");
            root.get_element_by_id("voitureASortir")
            ->set_attr("x", x).set_attr("y", y)
            .set_attr("width", width).set_attr("height", height);
        }else{
            //Sinon voiture normales
            *shapes<<SVG::Rect(x,y,width,height);
        }
}
SVG::SVG Situation::getSVG(){
    double x,y, width, height;
    SVG::SVG root;
    root.style("rect").set_attr("fill", "black");
    root.style("rect#voitureASortir").set_attr("fill", "red");
    root.style("rect#sortie").set_attr("fill", "green");
    for(int numeroV = 0; numeroV<nbVoiture; numeroV++){
        //On créer maintenant les variables pour ajouter la voiture au svg
        y = tabVoiture[numeroV]->PositionVoiture().ligne*100 + 5*tabVoiture[numeroV]->PositionVoiture().ligne;
        x = tabVoiture[numeroV]->PositionVoiture().colonne*100 + 5 * tabVoiture[numeroV]->PositionVoiture().colonne;
        if(tabVoiture[numeroV]->EstHorizontale()){
            width = tabVoiture[numeroV]->Longueur()*100;
            height = 1*100;
        }else{
            height = tabVoiture[numeroV]->Longueur()*100;
            width = 1*100;
        }
        MettreVoitureSVG(root,x,y,width,height, numeroV);
    }
    MettreSortieSVG(root);
    return root;
}

Situation* Situation::creationSituation(const int nbCoupMax){
    Situation* sit_ret = nullptr;
    Noeud* noeud_tmp;
    int nbCoup = 0;
    do{
        // On créer une situation aléatoire
        Situation *sit_tmp = new Situation();
        //On vérifie si elle est sortante
        Arbre a(sit_tmp);
        noeud_tmp = a.Simulation();
        //Si la situation est plus compliquée alors ça deviens notre situation maximale
        if(noeud_tmp->getHauteur()>= nbCoupMax){
            if(sit_ret != nullptr){
                delete sit_ret;
            }
            sit_ret = new Situation(*sit_tmp);
            nbCoup = noeud_tmp->getHauteur();
        }
    }while(nbCoup < nbCoupMax);
    std::cout<<"Nombre de coup "<<nbCoup<<std::endl;

    return sit_ret;
}

std::vector<Situation*> Situation::TabSituationDepuisDeplacementVoiture(){
    std::vector<Situation*> vec;
    //Pour chaque voiture il faut voir si on peut les déplacer et si c'est possible, mettre le résultat dans vec
    bool continuer = true;
    int cran;
    Situation* tmp;

    //On commence par faire une boucle pour parcourir toutes les voitures
    for(int i = 0; i<nbVoiture; i++){
        //Maintenant on aura 2 cas principal, la voiture se déplace dans un sens ou dans l'autre
        //Donc pour chaque voiture on commence par voir si on peut la déplacer dans le sens positif
        continuer = true;
        cran = 1;
        while(continuer){
            //Tant qu'on peut continuer on va tester si la voiture peut se déplacer d'un cran en + 
            tmp = SituationDepuisDeplacementVoiture(i, cran);
            //Si la situation est bien possible
            if(tmp!=nullptr){
                //Alors on la range dans notre tab de vecteur
                vec.push_back(tmp);
                cran++;
            }else{
                //Sinon c'est que le chemin est bloqué donc on ne peut pas continuer
                continuer = false;
            }
        }
        continuer = true;
        cran = -1;
        while(continuer){
            //Tant qu'on peut continuer on va tester si la voiture peut se déplacer d'un cran en -
            tmp = SituationDepuisDeplacementVoiture(i, cran);
            //Si la situation est bien possible
            if(tmp!=nullptr){
                //Alors on la range dans notre tab de vecteur
                vec.push_back(tmp);
                cran--;
            }else{
                //Sinon c'est que le chemin est bloqué donc on ne peut pas continuer
                continuer = false;
            }
        }
    }
    return vec;
}

void Situation::CreerVoiture(int ligne, int colonne, int longueur, bool horizontale){

    Position position;
    position.ligne = ligne;
    position.colonne = colonne; 
    tabVoiture[nbVoiture] = new Voiture(position, longueur, horizontale);
    nbVoiture++;
}

void Situation::CreerVoitureASortir(int ligne, int colonne, int longueur, bool horizontale){

    Position position;
    position.ligne = ligne;
    position.colonne = colonne; 
    tabVoiture[0] = new Voiture(position, longueur, horizontale);
    voitureASortir = tabVoiture[0];
    nbVoiture++;
}

void Situation::RemplirTabVoitureDepuisFichier(std::string path){
    std::ifstream fichier(path);

    //Si le fichier a bien été ouvert
    if(fichier){
        int ligne;
        int colonne;
        int longueur;
        bool horizontale;
        //On met la sortie
        fichier>>ligne>>colonne;
        sortie.ligne = ligne;
        sortie.colonne = colonne;

        //On met le vehicule qu'on doit sortir
        fichier>>ligne>>colonne>>longueur>>horizontale;
        CreerVoitureASortir(ligne, colonne, longueur, horizontale);

        //Tant qu'on peut créer des voitures on les remplis dans le tableau
        while(fichier>>ligne>>colonne>>longueur>>horizontale){
            CreerVoiture(ligne, colonne, longueur, horizontale);
        }

    }else{
        std::cout<<"on a pas reussis a ouvrir le fichier";
    }
}

void Situation::RemplirTabJeuDepuisTabVoiture(){
    //Pour chaque voiture dans le tableau de voiture, on remplis le tableau de jeu

    //On commence par la voiture qu'il faut sortir
    Voiture v;
    v = *tabVoiture[0];
    MettreVoitureASortirDansTabJeu(v);
    for(int i = 1; i<nbVoiture; i++){
        v = *tabVoiture[i];
        MettreVoitureDansTabJeu(v);
    }
}

void Situation::MettreVoitureASortirDansTabJeu(Voiture v){
    int ligne = v.PositionVoiture().ligne;
    int colonne = v.PositionVoiture().colonne;
    if(v.EstHorizontale()){
        for(int i = 0; i<v.Longueur(); i++){
            tabJeu[ligne][colonne + i] = 's';
        }
    }else{
        for(int i = 0; i<v.Longueur(); i++){
            tabJeu[ligne+i][colonne] = 's';
        }
    }
}
void Situation::MettreVoitureDansTabJeu(Voiture v){
    int ligne = v.PositionVoiture().ligne;
    int colonne = v.PositionVoiture().colonne;
    if(v.EstHorizontale()){
        for(int i = 0; i<v.Longueur(); i++){
            tabJeu[ligne][colonne + i] = '-';
        }
    }else{
        for(int i = 0; i<v.Longueur(); i++){
            tabJeu[ligne+i][colonne] = '|';
        }
    }
}

Situation* Situation::SituationDepuisDeplacementVoiture(int numeroVoiture, int nbDeplacement){
    //On regarde si la voiture peut se déplacer de nbDeplacement
    Voiture* voiture = tabVoiture[numeroVoiture];
    int x = voiture->PositionVoiture().colonne;
    assert(x >= 0);
    assert(x < 6);
    int y = voiture->PositionVoiture().ligne;
    assert(y >= 0);
    assert(y < 6);
    voiture = voiture;
    //Situation *sit = new Situation(*this);
    //On crée une situation en copie de la situation courante

    if(!voiture->EstHorizontale()){
        //Si la voiture est verticale on vérifie si on peut la déplacer dans la hauteur
        if((y+nbDeplacement+(voiture->Longueur()-1))>=6 || (y+nbDeplacement)<0){//(Si on ne dépasse pas les limites du tableau)
            return nullptr;
        }else if((tabJeu[y+nbDeplacement+(voiture->Longueur()-1)][x] != ' ')&&(nbDeplacement>=0)){//Sinon si il y a une voiture sur cette case
            //Alors on ne peut pas déplacer
            return nullptr;
        }else if ((tabJeu[y+nbDeplacement][x] != ' ')&&(nbDeplacement<0)){
            return nullptr;
        }else{
            Situation *sit = new Situation(*this);
            //Sinon ça veut dire qu'on peut déplacer la voiture
            //Dans la copie on a juste a changer les coordonés de la voiture qu'on a déplacé
            sit->tabVoiture[numeroVoiture]->SetPostitionVoiture(y+nbDeplacement, x);
            //Puis on change la position dans le tabJeu
            //On remet l'ancienne position à vide
            for(int i = y; i<y +voiture->Longueur(); i++){
                sit->tabJeu[i][x] = ' ';
            }
            //Maintenant on met la nouvelle voiture
            if(numeroVoiture==0){
                sit->MettreVoitureASortirDansTabJeu(*(sit->tabVoiture[numeroVoiture]));
            }else{
                sit->MettreVoitureDansTabJeu(*(sit->tabVoiture[numeroVoiture]));
            }
            //Situation* sitRet = new Situation(sit);
            //return sitRet;
            return sit;
        }
    }else{
        //Si la voiture est horizontale on vérifie si on peut la déplacer dans la largeur
        if((x+nbDeplacement+(voiture->Longueur()-1))>=6 || (x+nbDeplacement)<0){//(Si on ne dépasse pas les limites du tableau)
            return nullptr;
        }else if((tabJeu[y][x+nbDeplacement+(voiture->Longueur()-1)] != ' ')&&(nbDeplacement>=0)){//Sinon si il y a une voiture sur cette case
            //Alors on ne peut pas déplacer
            return nullptr;
        }else if ((tabJeu[y][x+nbDeplacement] != ' ')&&(nbDeplacement<0)){
            return nullptr;
        }else{
            Situation* sit = new Situation(*this);
            //Sinon ça veut dire qu'on peut déplacer la voiture
            //Dans la copie on a juste a changer les coordonés de la voiture qu'on a déplacé
            sit->tabVoiture[numeroVoiture]->SetPostitionVoiture(y, x+nbDeplacement);
            //Puis on change la position dans le tabJeu
            //On remet l'ancienne position à vide
            for(int i = x; i<x+voiture->Longueur(); i++){
                sit->tabJeu[y][i] = ' ';
            }
            //Maintenant on met la nouvelle voiture
            if(numeroVoiture==0){
                sit->MettreVoitureASortirDansTabJeu(*(sit->tabVoiture[numeroVoiture]));
            }else{
                sit->MettreVoitureDansTabJeu(*(sit->tabVoiture[numeroVoiture]));
            }
            //Situation* sitRet = new Situation(sit);
            //return sitRet;
            return sit;
        }
    }
}
std::string Situation::createKey(){
    std::string s;
    for(int i = 0; i<nbVoiture; i++){
        s += std::to_string(tabVoiture[i]->PositionVoiture().ligne);
        s += std::to_string(tabVoiture[i]->PositionVoiture().colonne);
        s += std::to_string(tabVoiture[i]->Longueur());
        s += std::to_string(tabVoiture[i]->EstHorizontale());
    }
    s += std::to_string(sortie.ligne);
    s += std::to_string(sortie.colonne);
    return s;
}
bool Situation::Est_Sortante(){
    bool ret = false;
    int x = voitureASortir->PositionVoiture().colonne;
    int y = voitureASortir->PositionVoiture().ligne;

    //Si le début de la voiture est sur la sortie 
    ret = ((sortie.ligne == y)&&(sortie.colonne == x));

    //Si la fin de la voiture est sur la sortie
    if(voitureASortir->EstHorizontale()){
        ret = ((sortie.ligne == y)&&(sortie.colonne == x + voitureASortir->Longueur()-1 ));
    }else{
        ret = ((sortie.ligne == y+ voitureASortir->Longueur()-1)&&(sortie.colonne == x ));
    }
    
    return ret;
}

void Situation::CreationSortieEtVoitureASortir(){
    //On décid où va être la sortie sur la longueur/largeur
    int random = rand()%6;

    //On décide sur quelle paroie est la sortie (longueur ou largeur)
    int sortiePositionnement = rand()%2;

    if (sortiePositionnement == 0){// Paroie droite
        //On positionne la sortie
        sortie.ligne = random;
        sortie.colonne = 5;

        //On positionne la voiture à sortir
        CreerVoitureASortir(random, 0, 2, 1);


    }else{//Paroie en haut
        sortie.ligne = 0;
        sortie.colonne = random;
        CreerVoitureASortir(4, random, 2, 0);
    }
    MettreVoitureASortirDansTabJeu(*tabVoiture[0]);

}
int Situation::tailleVoitureACreer(int ligne, int colonne, bool horizontale){
    int taille = -1;
    if(horizontale){
        //On test si on peut créer une voiture qui fait 2 de longueur
        if((colonne +1 <6)&&(tabJeu[ligne][colonne+1]==' ')){
            taille = 2;
            //Si on peut alors on regarde si on fait en sorte qu'elle soit de taille 3
            if(rand()%3 < 1){// (1 chance sur 3 qu'on test)
                if((colonne +2 <6)&&(tabJeu[ligne][colonne+2]==' ')){
                    taille = 3;
                }
            }
        }
    }else{
        if((ligne +1 <6)&&(tabJeu[ligne+1][colonne]==' ')){
            taille = 2;
            //Si on peut alors on regarde si on fait en sorte qu'elle soit de taille 3
            if(rand()%3 < 1){// (1 chance sur 3 qu'on test)
                if((ligne +2 < 6)&&(tabJeu[ligne+2][colonne]==' ')){
                    taille = 3;
                }
            }
        }
    }
    return taille;
}

void Situation::CreationVoitures(){
    nbVoiture = 1;
    int taille;
    int nombreDeVoitureMax = rand()%4+10;// On genère entre 8 et 15 voitures (on a déjà la voiture à sortir)
    /*for(int i = 0; i<6; i++){
        for(int j = 0; j<6; j++){
            if(rand()%4 >1){
                CreerVoiture(j, i, rand()%2+1,rand()%2);
                nombreDeVoitureMax--;
            }
            if (nombreDeVoitureMax<1){
                break;
            }
        }
    }*/
    //on va parcourir notre graph vide et créer les voitures à partir de ce dernier
    for(int i = 0; i<6; i++){
        for(int j=0; j<6; j++){
            //Si la case sur laquelle on veut créer notre voiture est vide alors on crée une voiture
            if(tabJeu[j][i]==' '){
                //On vérifie si on crée une voiture horizontale ou verticale
                bool horizontale = rand()%2;
                taille = tailleVoitureACreer(j,i,horizontale);
                if(taille >0){
                    CreerVoiture(j, i,taille,horizontale);
                    MettreVoitureDansTabJeu(*tabVoiture[nbVoiture-1]);
                    nombreDeVoitureMax--;
                }
                if(nombreDeVoitureMax<1){
                    return;
                }
            }
        }
    }
}