#include "noeud.h"
#include <vector>
#include <iostream>
#include <cstring>
#include <string>
Noeud::Noeud(){
    hauteur = 0;
    prec = nullptr;
    courante = nullptr;
}

Noeud::~Noeud(){
    hauteur = 0;
    if(courante != nullptr){
        delete courante;
        courante = nullptr;
    }
    for(std::size_t i = 0; i<suiv.size(); i++){
        delete suiv[i];
        suiv[i] = nullptr;
    }
    suiv.clear();
}

Noeud::Noeud(Noeud* p, Situation *c, int h){
    prec = p;
    courante = c;
    hauteur =  h;
}
int Noeud::getHauteur(){
    return hauteur;
}
void Noeud::AfficherParcours(){
    Noeud *n_parcours = this;
    while(n_parcours != nullptr){
        n_parcours->courante->Afficher();
        n_parcours = n_parcours->prec;
    }
}
void Noeud::AjoutTexteAnimation(SVG::SVG &animation){
    animation.style("text").set_attr("fill", "red")
    .set_attr("font", "500px").set_attr("height", "50px");

    animation.style("rect#pourTexte").set_attr("fill", "none")
    .set_attr("stroke","black");

    auto shapes= animation.add_child<SVG::Group>();
    std::string text = "Le nombre de coup pour résoudre ce puzzle est ";
    std::string text2 = std::to_string(getHauteur());
    *shapes<<SVG::Text(5,667,text + text2);
    shapes->add_child<SVG::Rect>("pourTexte");
    animation.get_element_by_id("pourTexte")
    ->set_attr("x", 5).set_attr("y", 650)
    .set_attr("width", 400).set_attr("height", 25);
}
void Noeud::AfficherParcoursSVG(){
    std::vector<SVG::SVG> frames;
    Noeud *n_parcours = this;
    while(n_parcours != nullptr){
        frames.push_back(n_parcours->courante->getSVG());
        n_parcours = n_parcours->prec;
    }

    SVG::SVG animation = SVG::frame_animate(frames, 1);
    AjoutTexteAnimation(animation);

    animation.autoscale();
    std::ofstream outfileAnimate("./ressource/image/Parcours.svg");
    std::cout<<"votre svg a été crée dans ./ressource/image/Parcours.svg"<<std::endl;
    outfileAnimate << std::string(animation);
}