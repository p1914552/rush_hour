#ifndef VOITURE_H
#define VOITURE_H
struct Position{
    int ligne;
    int colonne;
};

/**
* \class Voiture
* \brief Classe qui représente une voiture dans le jeu
*/
class Voiture{

    public : 

    /**
    * \fn Voiture(Position pos, int longueur, bool horizontale)
    * \brief Constructeur
    * \param pos int, La position de la voiture
    * \param longueur int, la longueur de la voiture
    * \param horizontale bool, est-ce que la voiture est horizontale
    */
    Voiture(Position position, int longueur, bool horizontale);

    /**
    * \fn Voiture()
    * \brief Constructeur
    */
    Voiture();

    /**
    * \fn ~Voiture()
    * \brief Desctructeur
    */
    ~Voiture();

    /**
    * \fn est_horizontale() const;
    * \brief Nous dis si la voiture est horizontale ou pas
    * \returns bool
    */
    bool EstHorizontale() const;

    /**
    * \fn longueur() const;
    * \brief Nous donne la longueur de la voiture
    * \returns int
    */
    int Longueur() const;

    /**
    * \fn PositionVoiture() const;
    * \brief Nous donne la position de la voiture
    * \returns Position
    */
    Position PositionVoiture() const;

    /**
    * \fn SetPostitionVoiture(int ligne, int colonne);
    * \brief Setter pour la position de la voiture
    * \param ligne integer, ligne sur laquelle la voiture est
    * \param colonne integer, colonne sur laquelle la voiture est
    */
    void SetPostitionVoiture(int ligne, int colonne);


    private : 
    Position pos;///Position de la voiture sous une stucture qui contient ligne et colonne
    bool est_horizontale;///Boolean pour savoir si la voiture est horizontale ou non
    int longueur;///Longueur de la voiture
};

#endif // VOITURE_H