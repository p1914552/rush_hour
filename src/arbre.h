#ifndef ARBRE_H
#define ARBRE_H
#include "noeud.h"
#include "situation.h"
#include <queue>
#include <string>
#include <unordered_map>
/**
* \class Arbre
* \brief Classe qui va nous permettre de parcrourir toutes les situations du jeu sous forme d'arbre
*/
class Arbre{
    public : 
    /**
    * \fn Arbre();
    * \brief constructeur de l'arbre
    */
    Arbre();

    /**
    * \fn Arbre(Situation s)
    * \brief constructeur de l'arbre en prenant en parametre une situation
    * \param s - Une situation a mettre dans le noeud racine de l'arbre
    */
    Arbre(Situation *s);

    /**
    * \fn ~Arbre()
    * \brief Destructeur de l'arbre
    */
    ~Arbre();

    /**
    * \fn Noeud* simulation
    * \brief va lancer la simulation pour résoudre le puzzle
    */
    Noeud* Simulation();

    /**
    * \fn Noeud* CreationSituation();
    * \brief Genère une situation aléatoire et renvoie la situation a partir de laquelle la resoudre est le plus long
    */
    Noeud* CreationSituation();
    //private :

    ///Pointeur de la racine de l'arbre
    Noeud* racine; 

    ///FIFO qui va nous permettre de parcourir l'arbre en largeur
    std::queue<Noeud*> fifo;
    
    ///Hashmap qui va nous permettre d'éviter d'ajouter des situations déjà existante dans la fifo
    std::unordered_map<std::string, Situation*> hashmap;



};
#endif // ARBRE_H