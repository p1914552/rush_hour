#include "voiture.h"


Voiture::Voiture(Position position, int longue, bool horizontale){
    pos = position;
    longueur = longue;
    est_horizontale = horizontale;
}

Voiture::Voiture(){
    pos.ligne = 0;
    pos.colonne = 0;
    longueur = 0;
    est_horizontale = 0;
}

Voiture::~Voiture(){
}

bool Voiture::EstHorizontale() const{
    return est_horizontale;
}

int Voiture::Longueur() const{
    return longueur;
}

Position Voiture::PositionVoiture() const{
    return pos;
}

void Voiture::SetPostitionVoiture(int ligne, int colonne){
    pos.ligne = ligne;
    pos.colonne = colonne;
}