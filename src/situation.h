#ifndef SITUATION_H
#define SITUATION_H
#include "voiture.h"
#include "svg.hpp"
#include <vector>
#include <string>
/**
* \class Situation
* \brief classe qui sert a stocker une situation du jeu Rush_Hour
*/
class Situation{

    public :
    /**
    * \fn Situation(std::string path)
    * \brief Constructeur depuis un fichier
    */
    Situation(std::string path);

    /**
    * \fn Situation(std::string path)
    * \brief Constructeur de sitaution aléatoire
    */
    Situation();

    /**
    * \fn Situation(const Situation& s)
    * \brief Constructeur par copie
    */
    Situation(const Situation &s);

    /**
    * \fn ~Situation()
    * \brief Desctructeur
    */
    ~Situation();

    /**
    * \fn void Afficher();
    * \brief Sert a afficher une situation dans la console 
    */
    void Afficher();

    /**
    * \fn void AfficherSVG();
    * \brief Sert à afficher une situation dans un SVG
    */
    void AfficherSVG();

    /**
    * \fn SVG getSVG();
    * \brief Recupère le svg qui correspond à une situation
    */
    SVG::SVG getSVG();

    /**
    * \fn static Situation* creationSituation(const int nbEssais);
    * \brief Fonction statique qui sert à créer une situation plus ou moins difficile par rapport a nbEssais
    * \returns Pointeur sur une situation
    */
    static Situation* creationSituation(const int nbEssais);

    /**
    * \fn Situation* SituationDepuisDeplacementVoiture(int numeroVoiture, int nbDeplacement);
    * \brief Renvoie une situation (si possible sinon renvoie nullptr) depuis un numéro de voiture et un nombre de déplacement
    * \param numeroVoiture integer, numéro de la voiture dans le tableau
    * \param nbDeplacement Nombre de déplacement a faire (dans la même direction)
    * \returns Un pointeur sur une situation
    */
    Situation* SituationDepuisDeplacementVoiture(int numeroVoiture, int nbDeplacement);
    /**
    * \fn std::vector<Situation*> TabSituationDepuisDeplacementVoiture();
    * \brief Renvoie toutes les autres situation qui découle de celle en parametre
    * \returns vecteur contenant toutes les situations découlant de celle qui appelle la fonction
    */
    std::vector<Situation*> TabSituationDepuisDeplacementVoiture();

    /**
    * \fn string createKey();
    * \brief Crée une clé unique de chaque situations
    * \return string, une clé pour la situation
    */
    std::string createKey();

    /**
    * \fn bool Est_Sortante();
    * \brief Nous dis si la situation est une situation sortante
    * \returns bool, Vrai si situation sortante, faux sinon
    */
    bool Est_Sortante();


    private :
    char tabJeu[6][6];///Tableau qui va nous servir a savoir si une case est remplie et a afficher des situations


    Voiture* tabVoiture[16];///Tableau contenant des pointeurs sur les voitures et les informations leur concernants

    Voiture *voitureASortir;///Pointeur sur la voiture a sortir

    Position sortie;///Position de la sortie

    int nbVoiture;///Nombre de voiture


    /**
    * \fn void CreerVoiture(int ligne, int colonne, int longueur, bool horizontale)
    * \brief Créer une voiture
    * \param ligne  integer, Ligne de la voiture
    * \param colonne integern, Colonne de la voiture 
    * \param longueur integer, Longueur de la voiture
    * \param horizontale boolean, Est-ce que la voiture est horizontale ?
    */
    void CreerVoiture(int ligne, int colonne, int longueur, bool horizontale);

        /**
    * \fn void CreerVoiture(int ligne, int colonne, int longueur, bool horizontale)
    * \brief Créer une voiture à sortir
    * \param ligne  integer, Ligne de la voiture
    * \param colonne integern, Colonne de la voiture 
    * \param longueur integer, Longueur de la voiture
    * \param horizontale boolean, Est-ce que la voiture est horizontale ?
    */
    void CreerVoitureASortir(int ligne, int colonne, int longueur, bool horizontale);

    /**
    * \fn void RemplirTabVoitureDepuisFichier(std::string path)
    * \brief On remplis le tableau de voiture depuis un fichier
    */
    void RemplirTabVoitureDepuisFichier(std::string path);

    /**
    * \fn void RemplirTabJeuDepuisTabVoiture()
    * \brief On remplis le tableau de jeu depuis le tableau de voiture
    */
    void RemplirTabJeuDepuisTabVoiture();
    /**
    * \fn void RemplirTabJeuDepuisTabVoiture()
    * \brief On remplis le tableau de jeu depuis le tableau de voiture
    */

    /**
    * \fn void MettreVoitureASortirDansTabJeu()
    * \brief On met la voiture a sortir dans le tableau de jeu
    */
    void MettreVoitureASortirDansTabJeu(Voiture v);

    /**
    * \fn void MettreVoitureDansTabJeu()
    * \brief On met une voiture dans le tableau de jeu
    */
    void MettreVoitureDansTabJeu(Voiture v);

    /**
    * \fn CreationSortieEtVoitureASortir();
    * \brief On crée une sortie avec une voiture a sortir
    */ 
    void CreationSortieEtVoitureASortir();

    /**
    * \fn void CreationVoitures();
    * \brief Créer des voitures aléatoirement qu'on rentre dans le jeu
    */
    void CreationVoitures();

    /**
    * \fn int tailleVoitureACreer(int ligne, int colonne);
    * \param ligne -int. Ligne de la position de la voiture
    * \param colonne -int. Colonne de la position de la voiture
    * \param horizontale -bool. Horizontalité de la voiture
    * \returns La taille de la position de la voiture à créer, -1 sinon
    * \brief On rentre une ligne et une colonne, nous donne la taille d'une voiture à créer à partir de ces coordonnés
    */
    int tailleVoitureACreer(int ligne, int colonne, bool horizontale);

    /**
    * \fn void AfficherSortieSVG(SVG root)
    * \brief affiche la sortie sous le format svg
    * \param SVG, root;
    */
    void MettreSortieSVG(SVG::SVG &root);
    /**
    * \fn void Situation::MettreVoitureSVG(SVG::SVG &root, double x, double y, double width, double height, int numeroV);
    * \brief pose la forme de la voiture dans le svg
    * \param SVG root. Root du SVG
    * \param double x. position x
    * \param double y. position y
    * \param double width.
    * \param double height.
    * \param int numeroV. le numéro de la voiture à poser
    */
    void MettreVoitureSVG(SVG::SVG &root, double x, double y, double width, double height, int numeroV);
};


#endif // SITUATION_H