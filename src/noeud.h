#ifndef NOEUD_H
#define NOEUD_H
#include <vector>
#include "situation.h"
/**
* \class Noeud
* \brief Noeud de l'arbre des possibilités des situations 
*/

class Noeud{
    public : 
    /**
    * \fn Noeud()
    * \brief Constructeur
    */
    Noeud();

    /**
    * \fn Noeud(Situation s)
    * \brief Constructeur qui prend en parametre une situation pour l'initialiser en situation courante et une situation precedente pour faire la même chose
    * \param prec - Situation precedente
    * \param courante - Situation courante
    * \param hauteur - hauteur souhaitée du noeud
    */
    Noeud(Noeud* prec,Situation *courante, int hauteur);


    /**
    * \fn ~Noeud()
    * \brief Destructeur
    */
    ~Noeud();

    /**
    * \fn int getHauteur();
    * \brief Retourne la hauteur du noeud
    * \returns int, La hauteur du noeud
    */
    int getHauteur();
    /**
    * \fn  void AfficherParcours()
    * \brief Affiche le parcour des situations d'un noeud dans la console
    */
    void AfficherParcours();

    /**
    * \fn  std::vector<SVG::SVG> AfficherParcoursSVG()
    * \brief Affiche le parcours des situations d'un noeud dans un .svg
    * \returns Un vecteur composé de SVG
    */
    void AfficherParcoursSVG();


    ///Pointeur vers le Noeud précédent
    Noeud* prec;

    ///La situation courante du noeud
    Situation* courante;

    ///Vecteur de pointeur de noeuds suivants
    std::vector<Noeud*> suiv;

    private : 
    ///La hauteur du noeud
    int hauteur;

    /**
    * \fn void AjoutTexteAnimation(SVG::SVG &animation)
    * \param animation SVG.
    */

    void AjoutTexteAnimation(SVG::SVG &animation);


};
#endif // NOEUD_H