#include <cassert>
#include "arbre.h"
Arbre::Arbre(){
    racine = nullptr; 
}

Arbre::Arbre(Situation* s){
    racine = new Noeud(nullptr, s, 0); 
}

Arbre::~Arbre(){
    if(racine != nullptr){
        delete racine;
        racine = nullptr;
    }
}
Noeud* Arbre::Simulation(){
    //Il faut que tant que la situation de la fifo n'est pas une simulation sortante
    //On continue de créer l'arbre et rajouter des situations dans la fifo
    Noeud* noeud_tmp = nullptr;
    std::vector<Situation*> vec_tmp;
    // On met le noeud racine dans la fifo
    fifo.push(racine);
    //On rajoute la situation de la racine dans la hashmap
    hashmap[racine->courante->createKey()] = racine->courante;
    std::string key;
    do{
        assert(!fifo.empty());
        noeud_tmp = fifo.front();
        fifo.pop();
        //On crée un tableau de vecteurs qui va contenir les situations suivantes du noeud temporaire
        vec_tmp = noeud_tmp->courante->TabSituationDepuisDeplacementVoiture();
        //Maintenant on regarde si la hash map contient chaque element de ce vecteur
        //Si elle ne contient pas un element, alor on l'insère dans la hashmap puis on transforme cette situation en noeud 
        //Qu'on va ensuite inseré dans la fifo et dans les succeseurs du noeud temporaire
        for (size_t i=0; i< vec_tmp.size(); i++){
            //Si cette situation n'existe pas dans la hashmap
            key = vec_tmp[i]->createKey();
            if(!hashmap.count(key)){
                //Alors on l'ajoute dans la hashmap
                hashmap[key] = vec_tmp[i];

                //Puis on crée un nouveau noeud qui contient cette situation qu'on va inserer dans les successeur du noeud temporaire
                //Et dans la fifo
                noeud_tmp->suiv.push_back(new Noeud(noeud_tmp, vec_tmp[i], noeud_tmp->getHauteur() +1));
                fifo.push(noeud_tmp->suiv[noeud_tmp->suiv.size()-1]);
            }else{
                delete vec_tmp[i];
            }
        }

    }while((!noeud_tmp->courante->Est_Sortante())&&(!fifo.empty()));
    if(fifo.empty()){
        //On veut que ce noeud créer sois quand même détruit avec l'arbre
        //On créer donc un noeud qu'on push au suivant du dernier noeud parcouru
        Noeud *n_tmp = new Noeud();
        noeud_tmp->suiv.push_back(n_tmp);
        noeud_tmp = n_tmp;
    }
    return noeud_tmp;
}
        
