#include <iostream>
#include "situation.h"
#include "arbre.h"
#include "svg.hpp"
#include <fstream>
#include <string.h>
int main(int argc, char *argv[]){
    srand (time(NULL));
    if((argc!=1)&&((argc==2)||(strcmp(argv[2],"SVG")!=0))){
        if(atoi(argv[1])==1){
            //On parcours et on trouve la meilleure solution pour l'énoncée
            Situation* s = new Situation("./ressource/situation/situationInitiale.txt");
            Arbre a(s);
            Noeud* n;
            n = a.Simulation();
            n->AfficherParcours();
            std::cout<<n->getHauteur()<<" coups"<<std::endl;
        }
        else if(atoi(argv[1])>=2){
            //Ce constructeur va créer une situaton plus ou moins compliquée
            Situation* s= Situation::creationSituation(atoi(argv[1]));
            //Ensuite on la résoud
            std::cout<<"Situation de base :"<<std::endl;
            s->Afficher();
            Arbre a(s);
            Noeud* n;
            n = a.Simulation();
            std::cout<<"Résolution pas à pas"<<std::endl;
            n->AfficherParcours();
            std::cout<<n->getHauteur()<<" coups"<<std::endl;
        }
    }
    else if((argc==3)||(strcmp(argv[2],"SVG")==0)){
        if(atoi(argv[1])==1){
            //On chargela situation du /txt
            Situation* s = new Situation("./ressource/situation/situationInitiale.txt");
            s->AfficherSVG();

            //On la résoud
            Arbre a(s);
            Noeud* n;
            n = a.Simulation();
            //On l'affiche
            n->AfficherParcoursSVG();
        }
        else if(atoi(argv[1])>=2){
            //Ce constructeur va créer une situaton plus ou moins compliquée
            Situation* s= Situation::creationSituation(atoi(argv[1]));
            //Ensuite on la résoud
            s->AfficherSVG();
            Arbre a(s);
            Noeud* n;
            n = a.Simulation();
            n->AfficherParcoursSVG();
        }
    }
    else{
        //Si on ne met pas de ligne de commande, on fait le parcours de base depuis le txt
        //On parcours et on trouve la meilleure solution pour l'énoncée
        Situation* s = new Situation("./ressource/situation/situationInitiale.txt");
        Arbre a(s);
        Noeud* n;
        n = a.Simulation();
        n->AfficherParcours();
        std::cout<<n->getHauteur()<<" coups"<<std::endl;
    }
    return 0;
}